Gdzie szukać wiedzy ?
- szkolenia
- konferencje
  - bardzo często przy konferencjach BigData są również prowadzone warsztaty i szkolenia
  - największe konferencje
    - Big Data Tech Summit (Warszawa)
    - DataWorks Summit
    - Strata Data Conference
    - PyData
    - Big Things Spain
    - Crunch Conference
    - InfoShare (Gdansk)
    - https://bigdataldn.com/
    - http://summit.datamass.io/ (Gdańsk)
    - https://www.dataengconf.com
    - https://dataops.barcelona/
    - konferencje również online:
      - Google Next
      - AWS Summit
- kursy online
  - http://udemy.com
  - http://edx.com
  - http://www.openuniversity.edu
  - https://www.coursera.org/
  - https://www.kaggle.com/
  - https://www.datacamp.com/
  - https://www.pluralsight.com/
- książki
- blogi firm, które mocno inwestują w technologię :)
- https://www.meetup.com/
- Java User Groups
- studia podyplomowe
- wirtualki / środowiska / jak się uruchomić ?
  - Databricks (free trial)
  - Cloudera / Hortonworks (https://www.cloudera.com/downloads/hortonworks-sandbox/hdp.html)
  - Google Cloud Platform (DataProc)
  - Amazon Web Services (EMR)
  - Microsoft Azure (HDInsight)
  - hub.docker.com
- dodatkowe źródła
  - Gartner
  - https://insights.stackoverflow.com/survey/2020



--------------------
Dziękuję za udział w szkoleniu :)
Arek Osiński
Kontakt:
arek.osinski@i-netpl.info
